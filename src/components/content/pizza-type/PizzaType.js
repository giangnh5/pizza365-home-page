import { Button, Card, CardActions, CardContent, CardMedia, Container, Grid, Typography } from "@mui/material"
import { useState } from "react";
const SelStyles = {
    width: 'auto',
    height: 'auto',
    m: 'auto',
    borderBottom: 1,
    borderColor: 'orange',
    borderWidth: '2px',
    textAlign: 'center',
    marginBottom: 4,
    marginTop: 2
};

const title = {
    fontSize: 'larger',
    fontWeight: '700',
    color: 'orange',
}

const PizzaType = ({pizzaType, setPizzaType}) => {
    const [colorS, setColorS] = useState('green')
    // Color size M
    const [colorM, setColorM] = useState('green')
    // Color size L
    const [colorL, setColorL] = useState('green')

    // SEAFOOD
    const onSeaFoodClick = () => {
        console.log('seafood')
        setColorS('gray');
        setColorM('green');
        setColorL('green');

        setPizzaType({
            loaiPizza: 'SEAFOOD',
        });
    }
    // HAWAII
    const onHawaiClick = () => {
        
        setColorS('green');
        setColorM('gray');
        setColorL('green');

        setPizzaType({
            loaiPizza: 'HAWWAII'
        })
    }
    // BACON
    const onBaconClick = () => {

        setColorS('green');
        setColorM('green');
        setColorL('gray');

        setPizzaType({
            loaiPizza: 'BACON'
        })
    }

    return (
        <>
            <Container>
                <Grid sx={SelStyles}>
                    <Typography sx={title} >
                        Chọn Loại Pizza
                    </Typography>
                </Grid>
                <Grid container spacing={5}>
                    
                    <Grid item xs={4}>
                        <Card sx={{ minWidth: 275 }}>
                            <CardMedia
                                component="img"
                                image={require('../../../assets/images/seafood.jpg')}
                            />
                            <CardContent>
                                <Grid item xs={12} padding={1}  >
                                    <Typography variant="h5">
                                    OCEAN MANIA
                                    </Typography>
                                </Grid>
                                <Grid item xs={12} padding={1}>
                                    <Typography variant="p">
                                    PIZZA HẢI SẢN SỐT MAYONNAISE
                                    </Typography>
                                </Grid>
                                <Grid item xs={12} padding={1} >
                                    <Typography variant="p">
                                    Tôm, Mực, Ớt ngọt, Hành tây, Cà chua, Xốt cà chua, Phomai Mozzarella.
                                    </Typography>
                                </Grid>
                            </CardContent>
                            <CardActions sx={{ background: 'orange'}}>
                                <Grid item xs={12} textAlign='center' >
                                    <Button onClick={onSeaFoodClick} variant="contained" sx={{background: colorS}}>
                                        Chọn
                                    </Button>
                                </Grid>
                            </CardActions>
                        </Card>
                    </Grid>
                    
                    <Grid item xs={4}>
                        <Card sx={{ minWidth: 275 }}>
                            <CardMedia
                                component="img"
                                image={require('../../../assets/images/hawaiian.jpg')}
                            />
                            <CardContent>
                                <Grid item xs={12} padding={1}  >
                                    <Typography variant="h5">
                                    HAWAIIAN
                                    </Typography>
                                </Grid>
                                <Grid item xs={12} padding={1}>
                                    <Typography variant="p">
                                    PIZZA DĂM BÔNG DỨA KIỂU HAWAII.
                                    </Typography>
                                    
                                </Grid>
                                <Grid item xs={12} padding={1} >
                                    <Typography variant="p">
                                    Xốt cà chua, Phomai Mozzarella, Thịt dăm bông, Dứa.                                        
                                    </Typography>                                    
                                </Grid>
                            </CardContent>
                            <CardActions sx={{ background: 'orange'}}>
                                <Grid item xs={12} textAlign='center' >
                                    <Button onClick={onHawaiClick} variant="contained" sx={{background: colorM}}>
                                        Chọn
                                    </Button>
                                </Grid>
                            </CardActions>
                        </Card>
                    </Grid>
                    
                    <Grid item xs={4}>
                        <Card sx={{ minWidth: 275 }}>
                            <CardMedia
                                component="img"
                                image={require('../../../assets/images/bacon.jpg')}
                            />
                            <CardContent>
                                <Grid item xs={12}  padding={1} >
                                    <Typography variant="h5" >
                                    CHEESY CHICKEN BACON
                                    </Typography>
                                </Grid>
                                <Grid item xs={12}  padding={1}>
                                    <Typography variant="p">
                                    PIZZA GÀ PHÔ MAI THỊT HEO XÔNG KHÓI
                                    </Typography>
                                </Grid>
                                <Grid item xs={12} padding={1}>
                                    <Typography variant="p" >
                                    Thịt ba chỉ xông khói, Thịt gà, Cà chua, Xốt cà chua, Phomai Mozzarella
                                    </Typography>
                                </Grid>
                            </CardContent>
                            <CardActions  sx={{ background: 'orange'}}>
                                <Grid item xs={12} textAlign='center' >
                                    <Button onClick={onBaconClick} variant="contained" sx={{background: colorL}}>
                                        Chọn
                                    </Button>
                                </Grid>
                            </CardActions>
                        </Card>
                    </Grid>
                </Grid>
            </Container>
        </>
    )
}

export default PizzaType