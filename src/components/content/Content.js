

import Info from "./About/About"
import Introduce from "./Introduce/Introduce"
import PizzaSize from "./pizza-size/PizzaSize"
import PizzaType from "./pizza-type/PizzaType"
import DrinkSelect from "./drink/DrinkSelect"
import FormSubmit from "./form/FormSubmit"

const Content = ({ pizzaSize, setPizzaSize, pizzaType, setPizzaType, valueDrink,setValueDrink, setAlert }) => {
    return(
        <>
            <Introduce />
            <Info />
            <PizzaSize
                pizzaSize={pizzaSize}
                setPizzaSize={setPizzaSize}
            />
            <PizzaType
                pizzaType={pizzaType}
                setPizzaType={setPizzaType}
            />
            <DrinkSelect 
            valueDrink={valueDrink} 
            setValueDrink={setValueDrink}
            />
            <FormSubmit
                pizzaType={pizzaType} 
                pizzaSize={pizzaSize} 
                valueDrink={valueDrink}
                setAlert={setAlert}
            />   
        </>
    )
}

export default Content